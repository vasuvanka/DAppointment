var mongoose    = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;
/*Users table structure*/
exports.Users = mongoose.model('Users',{
  'data': Object
},'Users');
exports.Admin = mongoose.model('Admin',{
  'username': String,
  'password': String,
  'lastLogin': Date
},'Admin');
exports.Private = mongoose.model('Private',{
  'data': Object
},'Private');

