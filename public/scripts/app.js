'use strict';

/**
 * @ngdoc overview
 * @name angularApp
 * @description
 * # angularApp
 *
 * Main module of the application.
 */
var app = angular
  .module('testApp', [
    // 'ngAnimate',
    // 'ngCookies',
    //'ngResource',
    'ngRoute',
    // 'ngSanitize',
    // 'ngTouch',
    // 'ui.bootstrap'
  ]);
app.config(function ($routeProvider,$httpProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/login.html',
        controller: 'loginCtrl',
        controllerAs: 'login'
      })
      .when('/home', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/private', {
        templateUrl: 'views/private.html',
        controller: 'PrivateCtrl',
        controllerAs: 'private'
      })
      .otherwise({
        redirectTo: '/'
      });
      delete $httpProvider.defaults.headers.common['X-Requested-With'];
  });

