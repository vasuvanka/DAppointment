'use strict';

/**
 * @ngdoc function
 * @name angularApp.controller:LoginCtrl
 * @description
 * # loginCtrl
 * Controller of the angularApp
 */
var app = angular.module('testApp');

app.controller('loginCtrl', function ($http,$scope,$location) {
	const baseUrl = "54.179.169.163:8081";
	//const baseUrl = "localhost:8081";
	$scope.validateLogin = function(pwd){
		if (pwd) {
			var req = {
					 method: 'POST',
					 url: ' http://'+baseUrl+'/api/v1/admin/login',
					 headers: {
					   'Content-Type': 'application/json'
					 },
					 data:{username:'mrmadhu',password:pwd}
				};
			$http(req).then(function(data){
				console.log(data);
				if (data.data.status == "success") {
					$location.path('/home')
				}
			}, function(err){

				$scope.error = 'invalid password';
				
			});
		}else{
			$scope.error = "Invalid Password";
		}
	}
});
