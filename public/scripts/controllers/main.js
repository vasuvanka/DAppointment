'use strict';

/**
 * @ngdoc function
 * @name angularApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the angularApp
 */
var app = angular.module('testApp');

app.controller('MainCtrl', function ($http,$scope,$location) {
	const baseUrl = "54.179.169.163:8081";
	// const baseUrl = "localhost:8081";
		$scope.menu0 = true;$scope.menu1 = false;$scope.menu2 = false;$scope.menu3 = false;
		$scope.showTab = function(tab){
			switch(tab){
				case "menu0":
					$scope.menu0 = true;
					$scope.menu1 = !$scope.menu0;
					$scope.menu2 = !$scope.menu0;
					$scope.menu3 = !$scope.menu0;
					break;
				case "menu1":
					$scope.menu1 = true;
					$scope.menu0 = !$scope.menu1;
					$scope.menu2 = !$scope.menu1;
					$scope.menu3 = !$scope.menu1;
					break;
				case "menu2":
					$scope.menu2 = true;
					$scope.menu1 = !$scope.menu2;
					$scope.menu0 = !$scope.menu2;
					$scope.menu3 = !$scope.menu2;
					break;
				case "menu3":
					$scope.menu3 = true;
					$scope.menu1 = !$scope.menu3;
					$scope.menu0 = !$scope.menu3;
					$scope.menu2 = !$scope.menu3;
			}
		};
		$scope.refresh = function(){
			getAllData();
		};
		function getAllData(){
			var req = {
				 method: 'GET',
				 url: ' http://'+baseUrl+'/api/v1/admin/users',
				 headers: {
				   'Content-Type': 'application/json'
				 }
			};
			$http(req).then(function(data){
				// alert(JSON.stringify(data));
				$scope.allData = data;
			}, function(err){
				$scope.allData.status = 'error';
				// alert(err);
			});
		}
    	getAllData();
		$scope.viewRec = {};
		$scope.assign = function(record){
			$scope.viewRec = record;
		};
		$scope.getRecord = function(id){
			var req = {
				 method: 'GET',
				 url: ' http://'+baseUrl+'/api/v1/admin/users/'+id,
				 headers: {
				   'Content-Type': 'application/json'
				 }
			};
			$http(req).then(function(data){
				// alert(JSON.Stringify(data));
				$scope.singleRec = data;
			}, function(err){
				$scope.singleRec.status = 'error';
				// alert(err);
			});
		};


  		$scope.open = function (rec) {
	      var x = confirm("Are you sure!");
		    if ( x == true) {
		        var req = {
					 method: 'PUT',
					 url: ' http://'+baseUrl+'/api/v1/admin/users/delete/'+rec._id,
					 headers: {
					   'Content-Type': 'application/json'
					 }
				};
				$http(req).then(function(data){
					getAllData();
				}, function(err){
					alert("unable to delete try again ..")
				});
		    } 
		  };
		$scope.updateRec = function (rec) {
	      var x = confirm("Are you sure!");
		    if ( x == true) {
		        var req = {
					 method: 'PUT',
					 url: ' http://'+baseUrl+'/api/v1/admin/users/'+rec._id,
					 headers: {
					   'Content-Type': 'application/json'
					 },
					 data:rec.data
				};
				$http(req).then(function(data){
					getAllData();
					alert("updated ...")
				}, function(err){
					alert("unable to update try again ..")
				});
		    } 
		  };
		  $scope.updateRecM1 = function (rec) {
	      var req = {
					 method: 'PUT',
					 url: ' http://'+baseUrl+'/api/v1/admin/users/'+rec._id,
					 headers: {
					   'Content-Type': 'application/json'
					 },
					 data:rec.data
				};
				$http(req).then(function(data){
					getAllData();
				}, function(err){
					alert("unable to update try again ..")
				});
		  };
		  $scope.savePrivateRec = function(rec){
		  	var req = {
				 method: 'POST',
				 url: ' http://'+baseUrl+'/api/v1/admin/private',
				 headers: {
				   'Content-Type': 'application/json'
				 },
				 data:rec.data
			};
			$http(req).then(function(data){
				// alert(JSON.stringify(data));
				data.data.status == "success" ? alert("data saved"):"";
			}, function(err){
				$scope.allData.status = 'error';
				alert("error");
				// alert(err);
			});
		  }

  });