var express = require('express');
var admin = express.Router();
var db = require('../model/admin');

admin.post('/users', function(req, res, next) {
	var data = req.body || null;
	if (data == null) {
		res.send({status:'error'});
	}else{
		data = typeof data == "string" ? JSON.parse(data) : data;
		var users = new db.Users({data:data});
		users.save(function (err,doc){
			if (err) {
				res.send({status:'error'});
			}else{
				res.send({status:'success',data:doc});
			}
		});
	}
});
admin.get('/users', function(req, res, next) {
	db.Users.find({},function (err,docs){
		if (err) {
			res.send({status:'error'});
		}else{
			res.send({status:'success',data:docs});
		}

	});
});
admin.get('/users/:id', function(req, res, next) {
	db.Users.findOne({'_id':req.params.id},function (err,docs){
		if (err) {
			res.send({status:'error'});
		}else{
			res.send({status:'success',data:docs});
		}
	});
});
admin.put('/users/delete/:id', function(req, res, next) {
	db.Users.remove({'_id':req.params.id},function (err,docs){
		if (err) {
			res.send({status:'error'});
		}else{
			res.send({status:'success',data:docs});
		}
	});
});
admin.put('/users/:id', function(req, res, next) {
	db.Users.update({'_id':req.params.id},{$set:{'data':req.body}},function (err,docs){
		if (err) {
			res.send({status:'error'});
		}else{
			res.send({status:'success',data:docs});
		}
	});
});
admin.post('/login', function(req, res, next) {
	if (req.body.username && req.body.password) {
		db.Admin.findOne({'username':req.body.username},function (err,docs){
			if (err) {
				res.send({status:'error'});
			}else{
				if (docs) {
					if (docs.password == req.body.password) {
						res.send({status:'success'});
					}else{
						res.send({status:'error'});
					}
					
				}else{
					res.send({status:'error'});
				}
			}
		});
	}else{
		res.send({status:'error'});
	}
});

admin.post('/private', function(req, res, next) {
	var data = req.body || null;
	if (data == null) {
		res.send({status:'error'});
	}else{
		data = typeof data == "string" ? JSON.parse(data) : data;
		var private = new db.Private({data:data});
		private.save(function (err,doc){
			if (err) {
				res.send({status:'error'});
			}else{
				res.send({status:'success',data:doc});
			}
		});
	}
});
admin.get('/private', function(req, res, next) {
	db.Private.find({},function (err,docs){
		if (err) {
			res.send({status:'error'});
		}else{
			res.send({status:'success',data:docs});
		}

	});
});
admin.get('/private/:id', function(req, res, next) {
	db.Private.findOne({'_id':req.params.id},function (err,docs){
		if (err) {
			res.send({status:'error'});
		}else{
			res.send({status:'success',data:docs});
		}
	});
});
admin.put('/private/delete/:id', function(req, res, next) {
	db.Private.remove({'_id':req.params.id},function (err,docs){
		if (err) {
			res.send({status:'error'});
		}else{
			res.send({status:'success',data:docs});
		}
	});
});
admin.put('/private/:id', function(req, res, next) {
	db.Private.update({'_id':req.params.id},{$set:{'data':req.body}},function (err,docs){
		if (err) {
			res.send({status:'error'});
		}else{
			res.send({status:'success',data:docs});
		}
	});
});

module.exports = admin;
